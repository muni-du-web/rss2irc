while true
date
do
  if [ -s muni-du-web.atom ]
    then
      etag=\"`grep ETag muni-du-web.atom|cut -d'"' -f2-`
      nonematch=If-None-Match:$etag
      echo "etag: $etag"
#      echo "nonematch: $nonematch"
#      echo ...
      wget --output-document=new-muni-du-web.atom --save-headers --header=''$nonematch'' http://gitorious.org/muni-du-web.atom
      if [ -s new-muni-du-web.atom ]
        then
#          echo diffing...
          grep '<title>' muni-du-web.atom > o-t.txt
          grep '<title>' new-muni-du-web.atom > n-t.txt
          diff -e o-t.txt n-t.txt |cut -d'>' -f2 -s|cut -d'<' -f1 > ~/irc/localhost/\#koumbit-mw/in
          mv muni-du-web.atom old-muni-du-web.atom
          mv new-muni-du-web.atom muni-du-web.atom
          rm o-t.txt n-t.txt
      fi
    else
      wget --save-headers http://gitorious.org/muni-du-web.atom
  fi
  sleep 15m
  date
done

